//Name: Divya Swamy
//Usage
// gcc tsig.c -o tsig.out  // convert file 
// chmod +x tsig.out // or make file executable
// for running   ./tsig.out 

#include <stdio.h>
#include <signal.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>

#define NUM_CHILD 10
//#define WITH_SIGNALS
#ifdef WITH_SIGNALS

bool interrupt = 0;

void keyboardInterrupt()
{
    printf("\nParent [%i]: The keyboard interrupt received.\n",getpid());
    interrupt = 1;
}
void SIGTERM_handler()
{
    printf("Child [%i]: Received SIGTERM signal, terminating\n", getpid());
}
#endif
int main()
{
    pid_t c_pid;//child process id
    pid_t p_pid=getpid();//assing parent process id
    int i,signal_num,count=0;
    printf("parent[%d]: The parent has started.\n",getpid());
#ifdef WITH_SIGNALS
        for(signal_num=0; signal_num<NSIG; signal_num++)  // NSIG - the total number of signals defined
        {//signals are allocated consecutively
            signal(signal_num,SIG_IGN);//igore all the signals
        }
        signal(SIGCHLD,SIG_DFL);//except child signals
        signal(SIGINT,keyboardInterrupt);//for SIGINT use function keyboardInterrupt
#endif
    for (i=0; i<NUM_CHILD; i++)//creation of all children
    {
        c_pid=fork();
        if (!c_pid) //we are in child process
        {
#ifdef WITH_SIGNALS
            signal(SIGINT,SIG_IGN);
            signal(SIGTERM,SIGTERM_handler);
#endif
            printf("Child [%d]:----^ Created\n",getpid());
            sleep(10);
            printf("Child [%d]: Execution completed\n",getpid());
            exit(0);//termination of the process
        }
        else if (c_pid==-1) //failure of creation
        {
            printf("Parent [%d]: Couldn't create new child.\n",getpid());
            kill(-getpgrp(),SIGTERM);//terminate all the processes in group
            exit(1);
        }
        else //we are in parent process
        {
            printf("Parent[%d] :--------!\n",getpid());//printing parent process id
        }
        sleep(1);
#ifdef WITH_SIGNALS
        if (interrupt)
        {
            printf("parent [%i]: Interrupt of the creation process!\n", getpid());
            printf("parent [%i]: Sending SIGTERM signal!\n", getpid());
            kill(-getpgrp(),SIGTERM);//terminate all the processes in group
            break;
        }
#endif
    }
    int status;
    while(1)
    {
        pid_t pid = wait(&status);//waiting for change of the status
        if(pid == -1)
        {
            printf("\nNo more processes to be synchronized.\n");
            break;
        }
        else
        {
            count++;//if there were a change of the status we increment count
        }
    }
    printf("\n%d Processes were terminated.\n", count);
#ifdef WITH_SIGNALS
    for(signal_num=0; signal_num<NSIG; signal_num++)//set default signal handling
        signal(signal_num,SIG_DFL);
#endif
    return status;
}
